
/*
   Given a string find all permutations of this string
   including duplicates

*/


#include<iostream>
using namespace std;
void swap(string &s, int i,int j)
 {
      char c = s[i];
        s[i]= s[j];
        s[j]= c;
 }
void p( string s,int i,int f )
 {    
    static int k=1;
      char c;
     if( i==f )
     {
        cout<<k<<" "; 
        k++; 
     	cout<<s<<endl;
     }
     else
     {

      for(  int j = i;j<=f;j++ )
          {
              swap(s,i,j);
              p(s,i+1,f); 
              swap(s,i,j); 
          }

     }	

 }
int main()
{
  string s;
  cout<<" enter a string"<<endl;
  cin>>s; 
  p(s,0,s.size()-1);	
	return 0;
}
