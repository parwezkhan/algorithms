
/*

  majority element using MOORE VOTING ALGORITHM

 time complexity  O(n)
    space O(1)

*/

#include<iostream>
using namespace std;
int main()
{
   int n,i,j;
   cout<<"  enter no. of elements \n ";
   cin>>n;
   int a[n];
   cout<<" enter elements \n ";
     for( i=0;i<n;i++ )
     	  cin>>a[i];

   //  finding most frequent elements 
           int v= a[0];
           int f= 1;
           for( i=1;i<n;i++ )
              {

                 if( a[i] == v )
                 	  f++;
                 else
                    f--;

                 if(  f== 0 )
                   {
                       v= a[i];
                       f=1;
                   }    	
              }  
    // now checking if v is majority elements
              int count=0;
          for( i=0;i<n;i++)
          {
          	if( v== a[i] )
          		count++;
      
          }  
            if( count > n/2  )
            	 cout<<" majority element is "<< v<<endl;
            else
               cout<< "  no majority element \n "<<endl;  	
	return 0;
}
