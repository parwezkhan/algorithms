
#include<iostream>
#include<queue>
#include<vector>
#define null 0
using namespace std;
struct node
       {
          char data ;
          int frequency;
          struct node *left,*right;
           node(  char data, int frequency  )
              {

                    this->data = data;
                    this->frequency= frequency;
                    this->left=this->right=null;

              }

       };
    struct cmp
    {
           bool operator()( node *l,node *r )
            {

            	return  l->frequency > r->frequency ;
            }


    }  ;
  void print( node * root,string s )
  {
        if( root ==null )
              return ;

        if( root->data != '#' )
              cout<< root->data << " " << s<<endl;

        print( root->left,s+'0' );
        print(root->right,s+'1');        
  } 
int main()
{
      int n,f;
      char v;
      priority_queue<  node *, vector< node *>, cmp > mh; // min heap as priority queue
      cout<< " enter no. of distinct data \n";
      cin>> n; 	
      cout<< "  enter data with their frequencies as data frequencies \n ";
        for( int i=0;i<n;i++)
        {
              cin>>v>>f;
              mh.push( new node( v,f ) );
        }
          struct node *l=null;
          struct node *r=null;
          struct node *t=null;
       while( mh.size() != 1 )
        {
            l= mh.top();
               mh.pop();
            r= mh.top();
               mh.pop();
            t = new node( '#',( l->frequency ) + ( r->frequency )  );
            t->left = l;
            t->right = r;       
            mh.push( t );
        }      
       string s="";
       print( mh.top(),s );  
}