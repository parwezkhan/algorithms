
/*

    finding maximum value of j-i such that A[i] <= A[j]

    

*/
#include<bits/stdc++.h>
bool cmp( const  pair< int,int> &x, const pair< int,int> &y   )
 {
    if( x.first == y.first  )
         return x.second < y.second ;
         
     return x.first < y.first ;     
 }
int maximumGap(const vector<int> &A) {
    vector< pair< int,int> > v;
      int n= A.size();
      for( int i=0;i< n;i++ )
           v.push_back( make_pair( A[i], i) );  // pair of value  keep  tracking index also
    
    sort( v.begin(),v.end(),cmp );  // sorting pair by value 
       
    int a[ n ];
    a[ n-1 ]= v[ n -1 ].second ;
    for( int i= n -2;i>=0 ; i-- )
        {
          a[ i ]= max( a[i+1], v[i].second );   // a[i] conatins greatest index greater than equal  index of pair i in sorted from right 
        }
    int ans= -1;
    for( int i=0;i< v.size(); i++ )
        {
            
          ans = max( ans, a[i] - v[i].second );    
        }
       return ans ;
}

