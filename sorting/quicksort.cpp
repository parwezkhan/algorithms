#include<iostream>
#include<malloc.h>
using namespace std;

int getpivot(  int *a, int i,int f )
{
   int pivot = a[i];

   while( i < f )
     {    
          
          while(  i<f && a[f] >pivot )	
                 f--;
          while( i< f && a[i] < pivot )
          	  i++;   
          swap(a[i],a[f]);   
     }     
    
    if( a[f] == pivot )
    	return f;
    else
    	return i;
}
void quicksort( int *a,int i,int f )
{
    if(  f-i  <1 )
    	return;

    int j= getpivot( a,i,f );
      quicksort( a,i,j);
      quicksort(a,j+1,f);
}
int main()
{
  int n,i;
  cout<<"  enter no. of element to sort  \n";
  cin>>n;
   int *a;
    a= ( int *)malloc(n*sizeof(int));
    cout<<"  enter elements \n ";
      for(i=0;i<n;i++)
          cin>>a[i];	

     quicksort(a,0,n-1);
      for(i=0;i<n;i++)
        cout<<a[i]<<" ";
	return 0;
}
