
/*
   minimum value in a rotated array   of a sorted array
    
    e.g.    array   1 2 3 4 5
           rotated  4 5 1 2 3
           min= 1


         you are given rotated array    
        
       time complexity  O(LOG(N))
*/
#include <iostream>
#include<vector>
using namespace std;
 int findMin(const vector<int> &A) {
            int low = 0, high = (int)A.size() - 1;
            int len = A.size();
            while (low <= high) {
                if (A[low] <= A[high]) return A[low]; // Case 1
                int mid = (low + high) / 2;
                int next = (mid + 1) % len, prev = (mid + len - 1) % len;
                if (A[mid] <= A[next] && A[mid] <= A[prev]) // Case 2
                    return A[mid];
                else if (A[mid] <= A[high]) high = mid - 1; // Case 3
                else if (A[mid] >= A[low]) low = mid + 1; // Case 4
            }
            return -1;
        }
int main()
{
  int n;
  cout <<" enter no. of elements \n"<<endl;
  cin>>n;
   vector<int>v(n);
   cout<<" enter elements in array "<<endl;
     for( int i=0;i<n;i++)
           cin>>v[i];

    cout<< findMin( v );

    return 0;
}

