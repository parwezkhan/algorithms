
/*

   find floor of square root of an integer without using sqrt()


*/

#include <iostream>
using namespace std;
int sqrt( int a )
{
if ( a==1 || a==0 )
     return a;
    int l= 1;
    int h= a;
    int ans;
    while( l<=h  )
    {
        long long m= (  long long )   ( h + l )/2LL ;
        if(  m*m == a  )
             {  ans=m;
                return ans;
             }
        else if( m*m < a )
            {
                ans=m;
                l=m+1;
            }
        else
           h= m-1;
    }
    return ans;
}
int main()
{   int n;
    cout<<"  enter any integer "<<endl;
    cin>>n;
    cout<< sqrt( n )<<endl;
    return 0;
}

