/*

lower bound and upper bound implementations 

*/

#include <iostream>
#include<vector>
using namespace std;
int lbound( vector< int > &v,int e )
{
    int l=0;
    int h= v.size(); // not n-1
    while( h > l )
    {
        int m= ( l + h)/2 ;
        if( v[m] >= e  )
        {
            h=m;
        }
        else
            l= m +1;
    }
      return l;
}
 int ubound( vector<int>& v, int e )
  {
      int l =0;
      int h = v.size();
      while( h > l )
      {
          int m= ( l + h)/2 ;
          if( v[m]<= e )
             l=m+1;
          else
              h= m;

      }
      return l;
  }
int main()
{
   int n;
   cout<<" enter no. of elements "<<endl;
   cin>>n;
   vector<int>v(n);
    cout<<" enter elements in vector "<<endl;
   for(int i=0;i<n;i++)
       cin>>v[i];

       int e;
       cout<<" enter value to search for lower bound "<<endl;
       cin>>e;
    int i= lbound( v,e );
    cout<< i<<endl;
       cout<<"  enter value to search for upper bound "<<endl;
       cin>>e;
    int j= ubound(  v, e);
    cout<< j<<endl;
    return 0;
}
