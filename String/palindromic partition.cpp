/*
all palindromic partition of a string

 ex NITIN
     N I T I N
    N ITI N
    NITIN

*/
#include <iostream>
#include<string>
#include<vector>
using namespace std;
bool ispalindrome( string s )
{
    int n = s.size();
    for( int i=0; i< n/2 ; i++ )
    {
        if( s[i] != s[ n-1 -i ] )
              return false;
    }
    return true;
}
void f( vector< vector< string > > &v, vector< string > &cur ,string s, int start , int n )
{
     if( start >= n ){
         v.push_back( cur );
         return ;
     }
    for( int i= start; i< n; i++ )
    {
        if( ispalindrome( s.substr(start,i-start +1 ) ))
        {
            cur.push_back( s.substr(start,i- start +1 ) );
            f( v,cur,s,i+1,n );
            cur.pop_back();
        }
    }
}
int main()
{
    string s;
    cout<<" enter a string "<<endl;
    cin>>s;
    vector< vector< string > >v;
    vector< string > cur;
     f( v, cur ,s,0, s.size() );
     for( int i=0;i< v.size(); i++)
           {
               for( int j=0; j< v[i].size(); j++)
                    cout<< v[i][j]<<" ";
                    cout<<endl;
           }
    return 0;
}
