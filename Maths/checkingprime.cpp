/*
  checking a prime number in O( sqrt(n))

*/
#include <iostream>
#include<math.h>
using namespace std;
bool isPrime( long long int n )
{
    if( n==0 || n==1 )
        return false;
    long long m= sqrt(n);
    for( long long int i=2;i<= m ; i++ )
          if( n % i ==0 )
               return false;

    return true;
}
int main()
{
    long long int n;
    cin>>n;
    if( isPrime(n) )
        cout<<" prime "<<endl;
    else
        cout<<" not prime "<<endl;
    return 0;
}
