/*

  find two unrepeated elements , all other repeated even number of times

  o(n)
  without extra space 
  
*/

#include <iostream>
using namespace std;
int main()
{
    int n;
    cout<<" enter no. of elements "<<endl;
    cin>>n;
    int a[n];
    cout<<" enter elements "<<endl;
    for( int i=0;i< n;i++)
      {
        cin>> a[i];
      }
      int x =0;
      for( int i=0;i< n;i++ )
          x = x ^ a[i] ;
      int index=0;
      for( int i=0;i< 32 ; i++ )
      {
          if( x & 1<< i  )
          {
              index = i;
              break;
          }

      }
      int y=0;
      for( int i=0;i< n;i++ )
      {
          if( a[i] & 1<< index )
               y= y ^ a[i];
      }
      cout << y << " " << ( x ^ y) ;

    return 0;
}
