
/*

Given an array of integers, every element appears k  times ( k > 1) except for one which occurs once.

Find that element which does not appear thrice.
   O(n)  ,constant extra space 


*/
class Solution {
public:
    int singleNumber(const vector<int> &A, int k) {
            int n = A.size();
        int count[32] = {0};
        int result = 0;
        for (int i = 0; i < 32; i++) {
            for (int j = 0; j < n; j++) {
                if ((A[j] >> i) & 1) {
                    count[i]++;
                }
            }
            result |= ((count[i] % k) << i); 
        }
        return result ;
    }
};


