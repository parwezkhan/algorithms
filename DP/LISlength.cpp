
/*  
    This is only for length of longest increasing subsequence not for actual sequence */


#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
int main() {
      int n;
      cin>>n;
       vector<int>v(n);
       for(int i=0;i<n;i++)
           cin>>v[i];
        vector< int>l;
         l.push_back(v[0]);  
    // list of LIS whose end pints are its elements , length of LIS will be index of these 
        for( int i=1;i<n;i++)
        {
          int j= lower_bound(l.begin(),l.end(),v[i])-l.begin();
           if( j< l.size() )
               l[j]=v[i];
           else
               l.push_back(v[i]);
        }
        cout<<l.size();
       
    return 0;
}
