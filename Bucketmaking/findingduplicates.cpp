/*

given an array read only  of size n+1
haning elements 1 to n
 and some duplicates find any duplicate
in O(n) time complexity
and space less than n
*/

#include <bits/stdc++.h>
using namespace std;
int f1(  vector< int>&v)
 {
    int n = v.size() -1;
    int bucketsize = sqrt( n );
    int lastbucketsize = bucketsize ;
    int numberofbucket = n / bucketsize ;
    if( bucketsize * bucketsize  != n ) // n is not perfact square
    {
         if( n % bucketsize !=0 )
         {
             numberofbucket +=1;
             lastbucketsize = n % bucketsize ;
         }
    }
    int bucket[ numberofbucket ];
    for( int i=0; i< numberofbucket ; i++ )
             bucket[i]=0;
       for( int i=0;i< v.size() ; i++ )
        {     int  a =  v[i];
              int  j =  ( a / bucketsize)  ;
              if( j* bucketsize != a )
                    j+=1;
              bucket[ j -1 ]+=1;
        }
      int bi ;  //  bucket having double
      int sz ; // size of that has to be
      for( int i=0;i< numberofbucket; i++ )
      {
          if( i== (numberofbucket-1) && bucket[i] > lastbucketsize )
          {
              bi = i;
              sz = lastbucketsize;
              break;
          }
          if( bucket[i] > bucketsize )
          {
              bi = i;
              sz= bucketsize ;
              break;
          }
      }
      int f= bi * bucketsize + 1; // first element of that bucket
      int l= f + sz -1;    // last bucket of that bucket
      map< int, int>m;
      for( int i=0;i< v.size(); i++ )
      {   int a= v[i];
          if( f<= a && a<= l )
          {
                if( m.find(a) != m.end() )
                      return a;
                m[a]= a;
          }
      }
 }
int main()
{
    int n;
    cout<<" enter no. of elements "<<endl;
    cin>>n;
    vector<int>v(n);
    for( int i=0;i< n;i++ )
    {
        cin>>v[i];
    }
      cout<<f1(v)<<endl;
    return 0;
}
