// power of a square matix   a^k in log(k) time  


vector<vector< long long>> mypow( vector<vector< long long>>&a, long long k )
 {
   long long n = a.size();
   long long i;
   vector<vector< long long >>b( n, vector< long long>(n) );  c
     for(i=0;i< n;i++)
          b[i][i]=1;         // creating identity matrix
     while( k>0)
      {
          if(k%2)
          b= mult(b,a);  // mult matrix multiplication function you can implement it easily
          k=k/2 ;
          a= mult(a,a);
      }   
     return b;
 }
